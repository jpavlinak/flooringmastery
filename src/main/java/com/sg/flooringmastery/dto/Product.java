/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Product {
    private String materialName;
    private double materialCostPerSqFt;
    private double laborCostPerSqFt;

	public Product() {
		
	}
	
	public Product(String materialName, double materialCostPerSqFt, double laborCostPerSqFt) {
		this.materialName = materialName;
		this.materialCostPerSqFt = materialCostPerSqFt;
		this.laborCostPerSqFt = laborCostPerSqFt;
	}
	
	@Override
	public String toString() {
		return "\nType: " + this.materialName
			 + "\nCost per sq. foot: " + this.materialCostPerSqFt
			 + "\nLabor cost per sq. foot: " + this.laborCostPerSqFt;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Product)) {
			return false;
		}
		
		Product that = (Product) obj;
		
		return this.materialName == that.materialName
			&& this.materialCostPerSqFt == that.materialCostPerSqFt
			&& this.laborCostPerSqFt == that.laborCostPerSqFt;
	}
	
    public String getMaterialName() {
        return this.materialName;
    }

    public double getMaterialCostPerSqFt() {
        return this.materialCostPerSqFt;
    }

    public double getLaborCostPerSqFt() {
        return this.laborCostPerSqFt;
    }
}
