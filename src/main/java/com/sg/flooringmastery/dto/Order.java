/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dto;

import java.text.DecimalFormat;

/**
 *
 * @author apprentice
 */
public class Order implements Cloneable {
    private int orderId;
    private String name;
    private Tax tax;
    private Product product;
    private double area;
    private double totalMaterialCost;
    private double totalLaborCost;
    private double totalTax;
    private double totalOrderCost;
	
	private final String pattern = "0.00";
	private final DecimalFormat dfOne = new DecimalFormat(pattern);

	public Order() {
		
	}
	
	public Order(String name, Tax tax, Product product, double area, double totalMaterialCost, double totalLaborCost, double totalTax, double totalOrderCost) {
		this.name = name;
		this.area = Double.parseDouble(dfOne.format(area));
		this.tax = tax;
		this.product = product;
		this.totalMaterialCost = Double.parseDouble(dfOne.format(totalMaterialCost));
		this.totalLaborCost = Double.parseDouble(dfOne.format(totalLaborCost));
		this.totalTax = Double.parseDouble(dfOne.format(totalTax));
		this.totalOrderCost = Double.parseDouble(dfOne.format(totalOrderCost));
	}
	
	public Order(int orderId, String name, Tax tax, Product product, double area, double totalMaterialCost, double totalLaborCost, double totalTax, double totalOrderCost) {
		this.orderId = orderId;
		this.name = name;
		this.area = Double.parseDouble(dfOne.format(area));
		this.tax = tax;
		this.product = product;
		this.totalMaterialCost = Double.parseDouble(dfOne.format(totalMaterialCost));
		this.totalLaborCost = Double.parseDouble(dfOne.format(totalLaborCost));
		this.totalTax = Double.parseDouble(dfOne.format(totalTax));
		this.totalOrderCost = Double.parseDouble(dfOne.format(totalOrderCost));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	@Override
	public String toString() {
		
		return "Order ID: " + this.orderId
				+ "\nName: " + this.name
				+ "\nState: " + this.tax.getState()
				+ "\nTax: " + dfOne.format(this.tax.getTaxRate())
				+ "\nMaterial: " + this.product.getMaterialName()
				+ "\nArea: " + dfOne.format(this.area)
				+ "\nCost per sq. foot: " + dfOne.format(this.product.getMaterialCostPerSqFt())
				+ "\nLabor per sq. foot: " + dfOne.format(this.product.getLaborCostPerSqFt())
				+ "\nTotal material cost: " + dfOne.format(this.totalMaterialCost)
				+ "\nTotal labor cost: " + dfOne.format(this.totalLaborCost)
				+ "\nTotal tax: " + dfOne.format(this.totalTax)
				+ "\nTotal cost: " + dfOne.format(this.totalOrderCost);
	}
	
	@Override
	public boolean equals(Object test) {
		if(!(test instanceof Order)) {
			return false;
		}
		
		Order that = (Order) test;
		
		return this.orderId == that.orderId
				&& this.name.equals(that.name)
				&& this.tax.getState().equals(that.tax.getState())
				&& this.tax.getTaxRate() == that.tax.getTaxRate()
				&& this.product.getMaterialName().equals(that.product.getMaterialName())
				&& this.area == that.area
				&& this.product.getMaterialCostPerSqFt() == that.product.getMaterialCostPerSqFt()
				&& this.product.getLaborCostPerSqFt() == that.product.getLaborCostPerSqFt()
				&& this.totalMaterialCost == that.totalMaterialCost
				&& this.totalLaborCost == that.totalLaborCost
				&& this.totalTax == that.totalTax
				&& this.totalOrderCost == that.totalOrderCost;
	}
	
	private double applyDoubleFormatting(double number) {
		
		double formatted = Double.parseDouble(dfOne.format(number));
		
		return formatted;
	}

	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getTotalMaterialCost() {
		return totalMaterialCost;
	}

	public void setTotalMaterialCost(double totalMaterialCost) {
		this.totalMaterialCost = totalMaterialCost;
	}

	public double getTotalLaborCost() {
		return totalLaborCost;
	}

	public void setTotalLaborCost(double totalLaborCost) {
		this.totalLaborCost = totalLaborCost;
	}

	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public double getTotalOrderCost() {
		return totalOrderCost;
	}

	public void setTotalOrderCost(double totalOrderCost) {
		this.totalOrderCost = totalOrderCost;
	}
}
