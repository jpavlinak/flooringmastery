/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {
	
	private final Scanner sc = new Scanner(System.in);

	public int getInt(String prompt) {
		return getInt(prompt, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY); }

	public int getInt(String prompt, float min, float max) {

		do {
			System.out.print(prompt);

			try {
				int userInput = Integer.parseInt(sc.nextLine());

				if(userInput >= min && userInput <= max) {
					return userInput; }

			} catch (NumberFormatException nfe) { }

			System.out.println("Invalid input. ");

		} while(true); }

	public long getLong(String prompt) {
		return getLong(prompt, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY); }

	public long getLong(String prompt, float min, float max) {

		do {
			System.out.print(prompt);

			try {
				long userInput = Long.parseLong(sc.nextLine());

				if(userInput >= min && userInput <= max) {
					return userInput; }

			} catch (NumberFormatException nfe) { }

			System.out.println("Invalid input. ");

		} while(true); }

	public double getDouble(String prompt) {
		return getDouble(prompt, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY); }

	public double getDouble(String prompt, float min, float max) {

		do {
			System.out.print(prompt);

			try {
				double userInput = Double.parseDouble(sc.nextLine());

				if(userInput >= min && userInput <= max) {
					return userInput; }

			} catch (NumberFormatException nfe) { }

			System.out.println("Invalid input. ");

		} while(true); }

	public float getFloat(String prompt) {
		return getFloat(prompt, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY); }

	public float getFloat(String prompt, float min, float max) {

		do {
			System.out.print(prompt);

			try {
				float userInput = Float.parseFloat(sc.nextLine());

				if(userInput >= min && userInput <= max) {
					return userInput; }

			} catch (NumberFormatException nfe) { }

			System.out.println("Invalid input. ");
			
		} while(true); }

	public String getString(String prompt) {
		System.out.print(prompt);
		
		return sc.nextLine(); }

	public void print(Object obj) {
		System.out.print(obj); }

	public void println(Object obj) {
		System.out.println(obj); }

}
