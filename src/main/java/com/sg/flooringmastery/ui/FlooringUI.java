/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class FlooringUI {
    
	private final ConsoleIO cio = new ConsoleIO();
	private final String fancyDelimiter = "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
	
	public void printMainMenu() {
		cio.println("\n=-=-=-= Main Menu =-=-=-="
				+ "\n1. Display Orders"
				+ "\n2. Add an Order"
				+ "\n3. Edit Order"
				+ "\n4. Remove Order"
				+ "\n5. Save All & Continue"
				+ "\n6. Save All & Exit"
				+ "\n7. Discard All & Exit");
	}
	
	public void printEditMenu() {
		cio.println("\n=-=-=-= Edit Menu =-=-=-="
				  + "\n1. Name"
				  + "\n2. State"
				  + "\n3. Material"
				  + "\n4. Area"
				  + "\n5. View Order"
				  + "\n6. Save & Exit"
				  + "\n7. Discard & Exit");
		cio.println("**Note** Any changes will not be saved to the database until 'Save' is selected from the Main Menu.");
	}
	
	public void printFarewell() {
		cio.println("\n" + fancyDelimiter);
		cio.println("Thank you for doing business with the J & P Flooring Emporium!");
		cio.println(fancyDelimiter);
	}
	
	public void printOrder(Order order) {
		cio.println("\n" + fancyDelimiter);
		cio.println(order);
		cio.println(fancyDelimiter);
	}
	
	public void printAllOrders(HashMap<Integer, Order> allOrders) {
		cio.println("\n" + fancyDelimiter);
		allOrders.values().stream().forEach(order -> cio.println(order + "\n" + fancyDelimiter));
	}
	
	public void printFlooringMaterials(List<Product> products) {
		cio.println("\n" + fancyDelimiter);
		products.stream().forEach(product -> cio.println(product));
		cio.println(fancyDelimiter);
	}
	
	public void printCostChangeNotice() {
		cio.println("\n**Note** Changing this field will alter the total cost of your order.");
	}
	
	public boolean getActionConfirmation(String prompt) {
		return cio.getInt("\n" + prompt
						+ "\n1. Yes"
						+ "\n2. No"
						+ "\n> ", 1, 2) == 1;
	}
}