/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class TaxDaoInMemImpl implements TaxDao {
	
	private ArrayList<Tax> taxRates;
	private final String TAX_FILE_DIRECTORY = "Data/Taxes.txt";
	private final String DELIMITER = "::";

	public TaxDaoInMemImpl() {
		taxRates = new ArrayList<>();
		loadAllTaxes();
	}
	
    @Override
    public void loadAllTaxes() {
        try {
			Scanner sc = new Scanner(new BufferedReader(new FileReader(TAX_FILE_DIRECTORY)));

			while(sc.hasNextLine()) {
				String taxLine = sc.nextLine();

				//index[0] = state, index[1] = taxRate
				String[] taxProperties = taxLine.split(DELIMITER);

				Tax tax = new Tax(taxProperties[0], Double.parseDouble(taxProperties[1]));
				taxRates.add(tax);
			}
		} catch(FileNotFoundException fnf) {
			
			try {
				new FileWriter(TAX_FILE_DIRECTORY);
			} catch(IOException ioe) { }
		}
    }

    @Override
    public Tax getTaxByState(String state) {
        List<Tax> specificTax = taxRates.stream().filter(tax -> tax.getState().equalsIgnoreCase(state)).collect(Collectors.toList());
		return specificTax.get(0);
	}

	@Override
	public List<Tax> getAllTaxes() {
		return taxRates;
	}
}
