/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class ProductDaoInMemImpl implements ProductDao {

	private ArrayList<Product> productList;
	private final String PRODUCT_FILE_DIRECTORY = "Data/Products.txt";;
	private final String DELIMITER = "::";
	
	public ProductDaoInMemImpl() {
		productList = new ArrayList<>();
		loadAllProducts();
	}
	
    @Override
    public void loadAllProducts() {
		try {
			Scanner sc = new Scanner(new BufferedReader(new FileReader(PRODUCT_FILE_DIRECTORY)));
			
			while(sc.hasNextLine()) {
				String productLine = sc.nextLine();
				
				//index[0] = material, index[1] = CostPerSqFoot, index[2] = LaborCostPerSqFoot
				String[] productProperties = productLine.split(DELIMITER);
				
				Product product = new Product(productProperties[0], Double.parseDouble(productProperties[1]), Double.parseDouble(productProperties[2]));
				productList.add(product);
			}
			
		} catch(FileNotFoundException fnf) {
			
			try {
				new FileWriter(PRODUCT_FILE_DIRECTORY);
			} catch(IOException ioe) { }
		}
	}

    @Override
    public List<Product> getAllProducts() {
		return productList;
	}

	@Override
	public Product getProductByType(String productName) {
		List<Product> chosenProduct = productList.stream().filter(product -> product.getMaterialName().equalsIgnoreCase(productName)).collect(Collectors.toList());
		return chosenProduct.get(0);
	}
    
}
