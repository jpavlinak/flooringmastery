/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.time.LocalDate;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class OrderDaoInMemImpl implements OrderDao {

	private HashMap<String, HashMap<Integer, Order>> storage;
	
	public OrderDaoInMemImpl() {
		storage = new HashMap<>();
	}
	
	
	@Override
	public boolean load(String date) {
		
		if(storage.containsKey(date)) {
			return true;
		}
		
		return false;
	}

	@Override
	public void write() {
		
	}

	@Override
	public void createNewOrder(Order newOrder) {
		/*
		 * 1. Create new key based on the current date
		 * 2. Check to see if the key is already a key in 'storage'
		 * 3. If it doesn't exist
		 *		Make a new hashmap to store ID/Order pairs
		 *		Set the orderID to 1.
		 *		Enter the new order into the newly created HashMap
		 * 4. If it does exist, get the associated HashMap full of orders.
		 *		Find the max ID.
		 *		set() the order's ID to max+1
		 *		put() this new order into the HashMap with an idKey of max+1
		 * 5. Put() this hashMap into storage using the filename key
		 */
		
		String today = LocalDate.now().toString();
		String[] yearMonthDay = today.split("-");
		
		String date = yearMonthDay[1] + yearMonthDay[2] + yearMonthDay[0];
		
		HashMap<Integer, Order> orders;
		
		if(!storage.containsKey(date)) {
			orders = new HashMap<>();
			
			newOrder.setOrderId(1);
			orders.put(1, newOrder);
		}
		
		else {
			orders = storage.get(date);
			
			int maxOrderID = orders.values().stream().mapToInt(Order::getOrderId).max().getAsInt();
			
			newOrder.setOrderId(maxOrderID + 1);
			orders.put(maxOrderID + 1, newOrder);
		}
		
		storage.put(date, orders);
	}

	@Override
	public HashMap<Integer, Order> getAllOrdersOnSpecificDate(String date) {
		/*
		 * 1. Create the key based on the passed date
		 * 2. Return the hashmap of all the orders associated with the filename/date the user entered
		 */
		return storage.get(date);
	}

	@Override
	public Order getOrder(String date, int orderID) {
		/*
		 * 1. Create the key based on the passed date
		 * 2. Access 'storage' using the filenam key
		 * 3. Access the value hashmap using the orderId variable
		 * 4. Return the found Order
		 */
		
		String key = date;
		HashMap<Integer, Order> orders = storage.get(key);
		
		try {
			return orders.get(orderID);
			
		} catch(NullPointerException npe) {
			return null;
		}
	}

	@Override
	public void updateOrder(String date, Order editedOrder) {
		/*
		 * 1. Create the key based on the passed date
		 * 2. get() the HashMap of orders from 'storage' based on the created filename
		 * 3. getOrderId() from the edited order
		 * 4. use the orderID to put() the edited order into the gotten hashMap
		 * 5. put() the hashmap of orders back into 'storage'
		 */
		
		HashMap<Integer, Order> orders = storage.get(date);
		int OrderID = editedOrder.getOrderId();
		orders.put(OrderID, editedOrder);
		
		//This isn't necessary, but I want to make sure the old order doesn't remain
		storage.put(date, orders);
	}

	@Override
	public boolean removeOrder(String date, int orderID) {
		/*
		 * 1. Create the key based on the passed date
		 * 2. get() the appropriate hashmaps of orders from 'storage' using the filename key
		 * 3. remove() the order using the passed in orderID
		 */
		
		HashMap<Integer, Order> orders = storage.get(date);
		
		//Avoids any annoying NullPointerExceptions
		if(orders.containsKey(orderID)) {
			orders.remove(orderID);
			return true;
		}
		
		//This isn't necessary, but I want to make sure the old order doesn't remain
		storage.put(date, orders);
		
		return false;
	}
}