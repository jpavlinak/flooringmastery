/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public interface OrderDao {
    
	public boolean load(String date);
    public void write();
    
    //C - Create
    public void createNewOrder(Order newOrder);
	
	//R - Read
	public HashMap<Integer, Order> getAllOrdersOnSpecificDate(String date);
	public Order getOrder(String date, int orderId);
    
	//U - Update
    public void updateOrder(String date, Order orderToEdit);

	//D - Delete
    public boolean removeOrder(String date, int orderId);
}