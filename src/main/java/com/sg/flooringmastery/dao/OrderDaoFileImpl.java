/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class OrderDaoFileImpl implements OrderDao {
    
	private final String BASE_FILE_NAME = "Orders/Orders_";
	private final String DELIMITER = "::";
	private HashMap<String, HashMap<Integer, Order>> storage;
	
	
	public OrderDaoFileImpl() {
		storage = new HashMap<>();
	}
	
	@Override
    public boolean load(String date) {
		/*
		 * 1. Create the file name based on the user supplied date
		 * 2. Check to see if the file name is already a key in 'storage'
		 * 3. If it doesn't exist, create a key/value pair, where the value is a
		 *		HashMap of orderID keys and corresponding Order values. Read in
		 *		the file and populate the new HashMap.
		 * 4. If it does exist, simply do nothing.
		 */
		
		String fileName = createFileNameKey(date);
		
		//if the fileName doesn't exist in memory
		if(!storage.containsKey(fileName)) {
			
			HashMap<Integer, Order> orders = new HashMap<>();
			
			try {
				Scanner sc = new Scanner(new BufferedReader(new FileReader(fileName)));
				
				while(sc.hasNextLine()) {
					String orderLine = sc.nextLine();
					String[] orderData = orderLine.split(DELIMITER);
					
					/*
					 * 0 = id, 1 = name, 2 = state, 3 = taxrate, 4 = productType, 5 = area, 6 = costpersqfoot
					 * 7 = laborpersqfoot, 8 = totalmaterialcost, 9 = totallaborcost, 10 = totaltax, 11 = totalcost
					 */
					int id = Integer.parseInt(orderData[0]);
					String name = orderData[1];
					Tax tax = new Tax(orderData[2], Double.parseDouble(orderData[3]));
					Product product = new Product(orderData[4], Double.parseDouble(orderData[6]), Double.parseDouble(orderData[7]));
					double area = Double.parseDouble(orderData[5]);
					double totalMaterialCost = Double.parseDouble(orderData[8]);
					double totalLaborCost = Double.parseDouble(orderData[9]);
					double totalTax = Double.parseDouble(orderData[10]);
					double totalCost = Double.parseDouble(orderData[11]);
					
					Order order = new Order(id, name, tax, product, area, totalMaterialCost, totalLaborCost, totalTax, totalCost);
					
					orders.put(id, order);
				}
				
				storage.put(fileName, orders);
				
			} catch(FileNotFoundException fnf) {
				return false;
			}
		}
		
		return true;
	}

	@Override
    public void write() {
        Set<String> fileNames = storage.keySet();
		PrintWriter pw;
		
		for(String fileName : fileNames) {
			HashMap<Integer, Order> orders = storage.get(fileName);
			Set<Integer> IDs = orders.keySet();
			
			try {
				pw = new PrintWriter(new FileWriter(fileName));
				
				for(Integer id : IDs) {
					Order currentOrder = orders.get(id);
					
					Tax tax = currentOrder.getTax();
					String state = tax.getState();
					double taxRate = tax.getTaxRate();
					
					Product product = currentOrder.getProduct();
					double laborCostPerSqFoot = product.getLaborCostPerSqFt();
					double materialCostPerSqFoot = product.getMaterialCostPerSqFt();
					String materialName = product.getMaterialName();
					
					double area = currentOrder.getArea();
					String name = currentOrder.getName();
					double totalLaborCost = currentOrder.getTotalLaborCost();
					double totalMaterialCost = currentOrder.getTotalMaterialCost();
					double totalOrderCost = currentOrder.getTotalOrderCost();
					double totalTax = currentOrder.getTotalTax();
					
					String encodedOrder = id + DELIMITER 
							+ name + DELIMITER
							+ state + DELIMITER
							+ taxRate + DELIMITER 
							+ materialName + DELIMITER 
							+ area + DELIMITER 
							+ materialCostPerSqFoot + DELIMITER 
							+ laborCostPerSqFoot + DELIMITER 
							+ totalMaterialCost + DELIMITER 
							+ totalLaborCost + DELIMITER 
							+ totalTax + DELIMITER 
							+ totalOrderCost;
					
					pw.println(encodedOrder);
				}
				
				pw.flush();
				pw.close();
				
			} catch(IOException ioe) {
				System.out.println("Error writing file: " + fileName);
			}
		}
    }

	@Override
    public void createNewOrder(Order newOrder) {
		
		String today = LocalDate.now().toString();
		String[] yearMonthDay = today.split("-");
		
		String date = yearMonthDay[1] + yearMonthDay[2] + yearMonthDay[0];
		String key = createFileNameKey(date);
		
		/*
		 * 1. Create the file name based on the current date
		 * 1a. load() the filename
		 * 2. Check to see if the file name is already a key in 'storage'
		 * 3. If it doesn't exist
		 *		Make a new hashmap to store ID/Order pairs
		 *		Set the orderID to 1.
		 *		Enter the new order into the newly created HashMap
		 * 4. If it does exist, get the associated HashMap full of orders.
		 *		Find the max ID.
		 *		set() the order's ID to max+1
		 *		put() this new order into the HashMap with an idKey of max+1
		 * 5. Put() this hashMap into storage using the filename key
		 */
		
		load(date);
		
		HashMap<Integer, Order> orders;
		
		//if the filename key doesn't exist
		if(!storage.containsKey(key)) {
			orders = new HashMap<>();
			
			newOrder.setOrderId(1);
			orders.put(1, newOrder);
		}
		
		//the filename key exists
		else {
			orders = storage.get(key);
			
			int maxOrderID = orders.values().stream().mapToInt(Order::getOrderId).max().getAsInt();
			
			newOrder.setOrderId(maxOrderID + 1);
			orders.put(maxOrderID + 1, newOrder);
		}
		
		storage.put(key, orders);
	}

	@Override
    public HashMap<Integer, Order> getAllOrdersOnSpecificDate(String date) {
		/*
		 * 1. Create the file name based on the passed date
		 * 2. Return the hashmap of all the orders associated with the filename/date the user entered
		 */
		String key = createFileNameKey(date);
		
		return storage.get(key);
    }
	
	@Override
    public Order getOrder(String date, int orderID) {
		/*
		 * 1. Create the file name based on the passed date
		 * 2. Access 'storage' using the filenam key
		 * 3. Access the value hashmap using the orderId variable
		 * 4. Return the found Order
		 */
		
		String key = createFileNameKey(date);
		HashMap<Integer, Order> orders = storage.get(key);
		
		try {
			return orders.get(orderID);
			
		} catch(NullPointerException npe) {
			return null;
		}
    }

	@Override
    public void updateOrder(String date, Order editedOrder) {
        /*
		 * 1. Create the filename based on the passed date
		 * 2. get() the HashMap of orders from 'storage' based on the created filename
		 *			- don't need to load(), since the appropriate file was already loaded to update an order
		 * 3. getOrderId() from the edited order
		 * 4. use the orderID to put() the edited order into the gotten hashMap
		 * 5. put() the hashmap of orders back into 'storage'
		 */
		String key = createFileNameKey(date);
		HashMap<Integer, Order> orders = storage.get(key);
		int orderID = editedOrder.getOrderId();
		
		orders.put(orderID, editedOrder);
		
		//This isn't necessary, but I want to make sure the old order doesn't remain
		storage.put(key, orders);
    }

	@Override
    public boolean removeOrder(String date, int orderID) {
        /*
		 * 1. Create the filename based on the passed date
		 * 2. get() the appropriate hashmaps of orders from 'storage' using the filename key
		 * 3. remove() the order using the passed in orderID
		 */
		String key = createFileNameKey(date);
		HashMap<Integer, Order> orders = storage.get(key);
		
		//Avoids any annoying NullPointerExceptions
		if(orders.containsKey(orderID)) {
			orders.remove(orderID);
			
			//This isn't necessary, but I want to make sure the old order doesn't remain
			storage.put(key, orders);
		
			return true;
		}
		
		return false;
    }
	
	private String createFileNameKey(String date) {
		return BASE_FILE_NAME + date + ".txt";
	}
}