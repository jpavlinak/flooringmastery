/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ops;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.ui.ConsoleIO;
import com.sg.flooringmastery.ui.FlooringUI;
import java.text.DecimalFormat;

/**
 *
 * @author apprentice
 */
public class FlooringController {
	
	private final FlooringBusinessLogic ops;
	private final ConsoleIO cio;
	private final FlooringUI ui;
	
	/*
	 * The following variables and constructor are part of Dependency Injection.
	 * It allows us to pass in DAOs depending on which implementation we
	 * want to use. 
	 * This requires the appropriate code within the App class.
	 */
	private OrderDao orderDao;
	private ProductDao productDao;
	private TaxDao taxDao;
	
	public FlooringController(OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
		this.orderDao = orderDao;
		this.productDao = productDao;
		this.taxDao = taxDao;
		
		this.ops = new FlooringBusinessLogic();
		this.cio = new ConsoleIO();
		this.ui = new FlooringUI();
	}
	
	public void run() {
		int mainMenuChoice;
		String date;
		int orderID;
		boolean ordersExist;
		
        do {
			//If there were any previous orders placed today, load them.
			String todaysDate = ops.getTodaysDate();
			orderDao.load(todaysDate);
					
			ui.printMainMenu();
			mainMenuChoice = cio.getInt("> ", 1, 7);
			
			switch(mainMenuChoice) {
				case 1: //Display all orders
					
					date = ops.getDateFromUser();
					ordersExist = orderDao.load(date);
					
					if(ordersExist) {
						ui.printAllOrders(orderDao.getAllOrdersOnSpecificDate(date));
					}
					
					else {
						cio.println("\nNo orders exist for that day.");
					}
					
					break;
					
				case 2: //add an order
					
					Order newOrder = createOrder();
					
					ui.printOrder(newOrder);
					
					if(ui.getActionConfirmation("Do you want to keep this order?")) {
						orderDao.createNewOrder(newOrder);
					}
					
					else {
						cio.println("\nOrder discarded.");
					}
					
					break;
					
				case 3: //edit an order
					
					date = ops.getDateFromUser();
					ordersExist = orderDao.load(date);
					
					if(ordersExist) {
						
						orderID = cio.getInt("\nPlease enter ID of the order you wish to edit: ", 1, Float.POSITIVE_INFINITY);
						Order order = orderDao.getOrder(date, orderID);
						
						if(order != null) {
							ui.printOrder(order);
							Order updatedOrder = editOrder(order);
							orderDao.updateOrder(date, updatedOrder);
						}
						
						else {
							cio.println("\nThat order ID doesn't exist.");
						}
					}
					
					else {
						cio.println("\nNo orders exist for that day.");
					}
					
					break;
					
				case 4: //remove an order
					
					date = ops.getDateFromUser();
					ordersExist = orderDao.load(date);
					orderID = cio.getInt("\nPlease enter ID of the order you wish to remove: ", 1, Float.POSITIVE_INFINITY);
					
					boolean isRemoveSuccessful = orderDao.removeOrder(date, orderID);
					
					if(ordersExist && isRemoveSuccessful) {
						cio.println("\nOrder successfully removed.");
					}
					
					else if(!ordersExist) {
						cio.println("\nNo orders exist for that day.");
					}
					
					else {
						cio.println("\nThat order ID doesn't exist.");
					}
					
					break;
					
				case 5: //save everything
				case 6: //save & quit
					orderDao.write();
					break;
				
				case 7: //discard & quit
				default: //should never be reached, but if it is somehow, simply quit the program.
			}
			
		} while(mainMenuChoice < 6);
		
		ui.printFarewell();
    }
	
	private Order createOrder() {
		
		String name = ops.getValidName("\nPlease enter your customer name: ", 1);
		String state = ops.getValidState("Please enter your state's 2-letter abbreviation: ", 1, taxDao.getAllTaxes());
		
		ui.printFlooringMaterials(productDao.getAllProducts());
		String productName = ops.getValidProduct("Please enter the desired material: ", 1, productDao.getAllProducts());
		
		Tax tax = taxDao.getTaxByState(state);
		Product product = productDao.getProductByType(productName);
		
		double area = ops.getValidArea("Please enter the area. Minimum of 1 sq. foot required: ", 1);
		double totalMaterialCost = ops.calcMaterialCost(area, product.getMaterialCostPerSqFt());
		double totalLaborCost = ops.calcLaborCost(area, product.getLaborCostPerSqFt());
		double totalTax = ops.calcTax(tax.getTaxRate(), totalMaterialCost, totalLaborCost);
		double totalOrderCost = ops.calcTotalCost(totalMaterialCost, totalLaborCost, totalTax);
		
		return new Order(name, tax, product, area, totalMaterialCost, totalLaborCost, totalTax, totalOrderCost);
	}
	
	private Order editOrder(Order order) {
		
		int editField;
		Order oldOrder;
		
		try {
			
			/*
			 * When I assign an existing object to a new variable, that variable references the original object.
			 * A new object is not created. Thus, if I edit the properties of the object in the new variable,
			 * the properties of the original are changed and lost. Therefore, I need to make a copy, a clone() if
			 * you will, of the original object in order to retain the unaltered properties of the original in case
			 * the user desires to discard all changes they made.
			 */
			
			oldOrder = (Order) order.clone();
			
		} catch(CloneNotSupportedException cns) {
			
			cio.println("\nAn error has occurred. Cannot edit this order.");
			return order;
		}
		
		Order updatedOrder = order;

		do {
			ui.printEditMenu();
			editField = cio.getInt("\nPlease enter your choice: ", 1, 7);

			switch(editField) {
				case 1: //edit name

					String newName = ops.getValidName("\nPlease enter the new customer name. Leave blank to keep the current value.\nCurrent name: " + updatedOrder.getName() + "\n> ", 0);

					if(!newName.equals("")) {
						updatedOrder.setName(newName);
					}

					break;

				case 2: //edit state

					ui.printCostChangeNotice();
					String newState = ops.getValidState("\nPlease enter your states 2-letter abbreviation. Leave blank to keep the current state."
							+ "\nCurrent state: " + updatedOrder.getTax().getState() + "\n> ", 0, taxDao.getAllTaxes());

					if(!newState.equals("")) {
						updatedOrder.setTax(taxDao.getTaxByState(newState));
					}

					break;

				case 3: //edit product

					ui.printFlooringMaterials(productDao.getAllProducts());
					ui.printCostChangeNotice();
					
					String newProduct = ops.getValidProduct("\nPlease enter a new product. Leave blank to keep the current product."
														  + "\nCurrent product: " + updatedOrder.getProduct().getMaterialName() + "\n> ", 0, productDao.getAllProducts());

					if(!newProduct.equals("")) {
						updatedOrder.setProduct(productDao.getProductByType(newProduct));
					}

					break;

				case 4: //edit area

					ui.printCostChangeNotice();
					double newArea = ops.getValidArea("\nPlease enter your new area in sq. feet. Enter '0' to keep the current area."
							+ "Current area in sq. feet: " + updatedOrder.getArea() + "\n> ", 0);

					if(newArea > 0) {
						updatedOrder.setArea(newArea);
					}

					break;

				case 5: //view order

					ui.printOrder(updatedOrder);
					break;

				case 6: //save & exit

					if(ui.getActionConfirmation("Are you sure you want to keep these changes?")) {
						return updatedOrder;
					}
					
					break;

				case 7: //discard & exit

					if(ui.getActionConfirmation("Are you sure you want to discard these changes?")) {
						return oldOrder;
					}
					
					break;
			}

			double totalMaterialCost = ops.calcMaterialCost(updatedOrder.getArea(), updatedOrder.getProduct().getMaterialCostPerSqFt());
			double totalLaborCost = ops.calcLaborCost(updatedOrder.getArea(), updatedOrder.getProduct().getLaborCostPerSqFt());
			double totalTax = ops.calcTax(updatedOrder.getTax().getTaxRate(), totalMaterialCost, totalLaborCost);
			double totalOrderCost = ops.calcTotalCost(totalMaterialCost, totalLaborCost, totalTax);

			updatedOrder.setTotalMaterialCost(totalMaterialCost);
			updatedOrder.setTotalLaborCost(totalLaborCost);
			updatedOrder.setTotalTax(totalTax);
			updatedOrder.setTotalOrderCost(totalOrderCost);

		} while(true);
	}
}