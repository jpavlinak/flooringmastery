/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ops;

import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.ui.ConsoleIO;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class FlooringBusinessLogic {
	
	ConsoleIO cio = new ConsoleIO();
	
    public double calcTax(double taxRate, double materialCost, double laborCost) {
        return (taxRate / 100) * (materialCost + laborCost);
    }
    
    public double calcMaterialCost(double area, double materialCostPerSqFt) {
        return area * materialCostPerSqFt;
    }
    
    public double calcLaborCost (double area, double laborCostPerSqFt) {
        return area * laborCostPerSqFt;
    }
    
    public double calcTotalCost (double materialCost, double laborCost, double tax) {
        return materialCost + laborCost + tax;
    }
	
	public String getValidProduct(String prompt, int minLength, List<Product> products) {
		do {
			String userProduct = cio.getString("\n" + prompt);
			List<Product> productMatch = products.stream().filter(product -> product.getMaterialName().equalsIgnoreCase(userProduct)).collect(Collectors.toList());

			if(minLength > 0) {
				
				if(productMatch.size() == 1) {
					return productMatch.get(0).getMaterialName();
				}
			}
			
			else {
				if(userProduct.length() > minLength) {
					if(productMatch.size() == 1) {
						return (productMatch.get(0).getMaterialName());
					}
				}
				
				else if(userProduct.length() == 0) {
					return "";
				}
			}
			
			cio.println("\nInvalid entry.");
			
		} while(true);
	}
	
	public String getValidState(String prompt, int minLength, List<Tax> taxes) {
		do {
		
			String userState = cio.getString("\n" + prompt);
			List<Tax> taxMatch = taxes.stream().filter(tax -> tax.getState().equalsIgnoreCase(userState)).collect(Collectors.toList());
			
			if(minLength > 0) {
				if(userState.length() >= minLength) {

					if(taxMatch.size() == 1) {
						return taxMatch.get(0).getState();
					}
				}
			}
			
			else {
				if(userState.length() > minLength) {

					if(taxMatch.size() == 1) {
						return taxMatch.get(0).getState();
					}
				}

				else if(userState.length() == 0) {
					return "";
				}
			}
			
			cio.println("\nInvalid entry.");
			
		} while(true);
	}
	
	public double getValidArea(String prompt, int minArea) {
		cio.println("\n**Note** Area cannot be greater than 500 sq. feet. If more than"
				  + "\n500 sq. feet is required, please place a second order with us.");
		return cio.getDouble("\n" + prompt, minArea, 500);
		
	}
	
	public String getValidName(String prompt, int minLength) {
		
		do {
			String userName = cio.getString("\n" + prompt);
			
			if(minLength > 0) {
				if(userName.length() >= minLength) {
					return userName;
				}
			}
			
			else {
				if(userName.length() > minLength) {
					return userName;
				}
				
				else if(userName.length() == 0) {
					return "";
				}
			}
			
			cio.println("\nInvalid entry.");
			
		} while(true);
	}
	
	public String getDateFromUser() {
		
		do {
			String dateInput = cio.getString("\nPlease enter a date in the form of yyyy-mm-dd: ");
		
			try {
				LocalDate userInput = LocalDate.parse(dateInput);
				
				int year = userInput.getYear();
				int month = userInput.getMonthValue();
				int day = userInput.getDayOfMonth();
				
				StringBuilder date = new StringBuilder();
				
				return date.append(month < 10 ? "0" : "").append(month).append(day < 10 ? "0" : "").append(day).append(year).toString();
				
			} catch(DateTimeParseException dtp) {
				cio.println("\nInvalid date or format. Please try again.");
			}
		} while(true);
	}
	
	public String getTodaysDate() {
		String today = LocalDate.now().toString();
		String[] yearMonthDay = today.split("-");
		
		return yearMonthDay[1] + yearMonthDay[2] + yearMonthDay[0];
	}
}
