/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.tests;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class OrderDaoTests {
	
	private TaxDao taxDao;
	private ProductDao productDao;
	private OrderDao orderDao;
	
	private final Order orderOne = new Order("Pavlinak", new Tax("OH", 5.75), new Product("Wood", 5.15, 4.75), 125.0, 643.75, 593.75, 71.15625, 1308.65625);
	private final Order orderTwo = new Order("Snafu", new Tax("IN", 7.0), new Product("Laminate", 1.75, 2.10), 200.0, 350.0, 420.0, 46.2, 816.2);
	private final Order orderThree = new Order("Chesterland", new Tax("MI", 6.0), new Product("Carpet", 2.25, 2.10), 200.0, 450.0, 420.0, 52.2, 922.2);	
	private final Order orderFour = new Order(1, "Thompson", new Tax("WY", 4.0), new Product("Tile", 3.5, 4.15), 50.0, 175.0, 207.5, 15.3, 397.8);
	private final Order orderFive = new Order(2, "Rahl", new Tax("CA", 7.5), new Product("Wood", 5.15, 4.75), 72.6, 373.89, 344.85, 53.9055, 772.6455);
	
	private final Order newOrderWithoutID = new Order("Raol", new Tax("KS", 6.5), new Product("Carpet", 2.25, 2.10), 98.7, 222.075, 207.27, 27.907425, 457252425);
	private final Order newOrderWithID = new Order(1, "Raol", new Tax("KS", 6.5), new Product("Carpet", 2.25, 2.10), 98.7, 222.075, 207.27, 27.907425, 457252425);

	private final Order updatedOrder = new Order(1, "Garcia", new Tax("IA", 6.5), new Product("Carpet", 2.25, 2.10), 98.7, 222.075, 207.27, 27.907425, 457252425);
	
	
	public OrderDaoTests() {
	}
	
	@Before
	public void setUp() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		taxDao = (TaxDao) ctx.getBean("taxTest");
		productDao = (ProductDao) ctx.getBean("productTest");
		orderDao = (OrderDao) ctx.getBean("orderTest");
	}

	@Test
	public void testEmptyOrderDao() {
		assertNull(orderDao.getAllOrdersOnSpecificDate("10122016"));
	}
	
	@Test
	public void testEmptyOrderDao_AddEditRemoveOneOrder() {
		
		orderDao.createNewOrder(newOrderWithoutID);
		
		String today = LocalDate.now().toString();
		String[] yearMonthDay = today.split("-");
		
		String date = yearMonthDay[1] + yearMonthDay[2] + yearMonthDay[0];
		
		//testing getAllOrders()
		HashMap<Integer, Order> todaysOrders = orderDao.getAllOrdersOnSpecificDate(date);
		
		//assert there is only one order
		assertEquals(1, todaysOrders.size());
		
		Set<Integer> orderIDs = todaysOrders.keySet();
		
		//assert the order's id == 1
		assertTrue(orderIDs.contains(1));
		assertFalse(orderIDs.contains(0));
		assertFalse(orderIDs.contains(2));
		
		//testing getOrder()
		Order order = orderDao.getOrder(date, 1);
		assertTrue(newOrderWithID.equals(order));
		
		//testing updateOrder()
		assertFalse(newOrderWithID.equals(updatedOrder)); //ensure they're different
		
		orderDao.updateOrder(date, updatedOrder);
		Order shouldBeUpdated = orderDao.getAllOrdersOnSpecificDate(date).get(1); //this should equal updatedOrder
		assertEquals(updatedOrder, shouldBeUpdated);
		
		//there should still be only one order
		assertEquals(1, todaysOrders.size());
		
		//testing remove()
		todaysOrders.remove(1);
		
		assertEquals(0, todaysOrders.size());
	}
	
	@Test
	public void testOrderDao_OneDaySeveralOrders() {
		String today = LocalDate.now().toString();
		String[] yearMonthDay = today.split("-");
		
		String date = yearMonthDay[1] + yearMonthDay[2] + yearMonthDay[0];
		
		orderDao.createNewOrder(orderOne);
		orderDao.createNewOrder(orderTwo);
		orderDao.createNewOrder(orderThree);
		orderDao.createNewOrder(orderFour);
		orderDao.createNewOrder(orderFive);
		
		HashMap<Integer, Order> orders = orderDao.getAllOrdersOnSpecificDate(date);
		
		//did the all the orders get added?
		int numberOfOrders = orders.size();
		assertEquals(5, numberOfOrders);

		//test to make sure the ids are assigned as expected
		assertNull(orderDao.getOrder(date, 0));
		assertNull(orderDao.getOrder(date, 6));
		
		for(int id = 1; id <= orders.size(); id++) {
			assertTrue(orders.containsKey(id));
		}
		
		//remove an order, test again
		orderDao.removeOrder(date, 3);
		HashMap<Integer, Order> reducedOrders = orderDao.getAllOrdersOnSpecificDate(date);
		
		int fewerNumberOfOrders = reducedOrders.size();
		assertEquals(4, fewerNumberOfOrders);
		assertNull(orderDao.getOrder(date, 3));
		
		//test adding another order. how does the id fare?
		orderDao.createNewOrder(newOrderWithoutID);
		HashMap<Integer, Order> newOrderAdded = orderDao.getAllOrdersOnSpecificDate(date);
		
		int greaterNumberOfOrders = newOrderAdded.size();
		assertEquals(5, greaterNumberOfOrders);
		assertNull(orderDao.getOrder(date, 3));
		
		assertNotNull(orderDao.getOrder(date, 6));
	}
}