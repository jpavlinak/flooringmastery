/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.tests;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class ProductDaoTests {
	
	private TaxDao taxDao;
	private ProductDao productDao;
	private OrderDao orderDao;
	
	private final double DELTA = 1E-7;
	
	@Before
	public void setUp() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		taxDao = (TaxDao) ctx.getBean("taxTest");
		productDao = (ProductDao) ctx.getBean("productTest");
		orderDao = (OrderDao) ctx.getBean("orderTest");
	}
	
	@Test
	public void testProductDao() {
		//do they exist?
		assertNotNull(productDao.getProductByType("Wood"));
		assertNotNull(productDao.getProductByType("Laminate"));
		assertNotNull(productDao.getProductByType("Carpet"));
		assertNotNull(productDao.getProductByType("Tile"));
		
		//Make sure the data isn't being jumbled between products
		assertEquals(2.25, productDao.getProductByType("Carpet").getMaterialCostPerSqFt(), DELTA);
		assertEquals(2.10, productDao.getProductByType("Carpet").getLaborCostPerSqFt(), DELTA);
		
		//are there 4 products?
		assertEquals(4, productDao.getAllProducts().size());
		
		//it's assumed that any data getting to a dao is legitimate data, so i can't use this test
		//assertNull(productDao.getProductByType("Dust"));
	}
}
