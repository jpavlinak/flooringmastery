/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.tests;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class TaxDaoTests {
	
	private TaxDao taxDao;
	private ProductDao productDao;
	private OrderDao orderDao;
	
	private final double DELTA = 1E-7;
	
	@Before
	public void setUp() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		taxDao = (TaxDao) ctx.getBean("taxTest");
		productDao = (ProductDao) ctx.getBean("productTest");
		orderDao = (OrderDao) ctx.getBean("orderTest");
	}
	
	@Test
	public void testTaxDao() {
		//did all 50 states get loaded?
		assertEquals(50, taxDao.getAllTaxes().size());
		
		//Make sure the data isn't being jumbled between products
		assertNotNull(taxDao.getTaxByState("OH"));
		assertEquals(5.75, taxDao.getTaxByState("OH").getTaxRate(), DELTA);
	}
}
